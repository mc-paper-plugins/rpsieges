package dev.bastitime.plugins.rpsieges;

import dev.bastitime.plugins.rpsieges.commands.SiegesCommands;
import dev.bastitime.plugins.rpsieges.manager.TeamManager;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPIBukkitConfig;
import net.kyori.adventure.platform.bukkit.BukkitAudiences;
import org.bukkit.plugin.java.JavaPlugin;
import org.checkerframework.checker.nullness.qual.NonNull;

public final class RPSieges extends JavaPlugin {

    private BukkitAudiences adventure;

    public @NonNull BukkitAudiences adventure() {
        if(this.adventure == null){
            throw new IllegalStateException("Tried to access Adventure when the plugin is disabled!");
        }
        return this.adventure;
    }

    @Override
    public void onLoad() {
        //Load CommandAPI
        CommandAPI.onLoad(new CommandAPIBukkitConfig(this).verboseOutput(true)); // Load with verbose output
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.adventure = BukkitAudiences.create(this);
        //Enable CommandAPI
        CommandAPI.onEnable();


        SiegesCommands siegesCommands = new SiegesCommands(this, new TeamManager());
        siegesCommands.registerAllCommands();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        if(this.adventure != null) {
            this.adventure.close();
            this.adventure = null;
        }
    }
}
