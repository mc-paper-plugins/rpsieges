package dev.bastitime.plugins.rpsieges;

import net.kyori.adventure.text.Component;

public class STATICS {

    public static String prefix = "<#212121>[<#ab3f27>Sieges<#212121>] ";

    public static String white = "<#ededed>";
    public static String lightGray = "<#9c9c9c>";
    public static String darkGray = "<#212121>";
    public static String red = "<#d62a1e>";
    public static String softRed = "<#bd5c4b>";
    public static String green = "<#1cad43>";
}
