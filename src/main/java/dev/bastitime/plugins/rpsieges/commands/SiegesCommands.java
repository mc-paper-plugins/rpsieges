package dev.bastitime.plugins.rpsieges.commands;

import dev.bastitime.plugins.rpsieges.RPSieges;
import dev.bastitime.plugins.rpsieges.manager.TeamManager;
import dev.bastitime.plugins.rpsieges.models.Siege;
import dev.bastitime.plugins.rpsieges.models.Team;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.CommandTree;
import dev.jorel.commandapi.arguments.CommandArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class SiegesCommands {

    // Plugin reference in case we need to access anything about the plugin,
    // such as its config.yml or something
    private RPSieges plugin;
    private TeamCommands teamCommands;

    private TeamManager teamManager;

    public SiegesCommands(RPSieges plugin, TeamManager teamManager) {
        this.plugin = plugin;
        this.teamManager = teamManager;
        this.teamCommands = new TeamCommands(plugin, teamManager);
    }

    public void registerAllCommands() {
        // /break <location>
        // Breaks a block at <location>. Can only be executed by a player
        new CommandAPICommand("sieges")
                .withSubcommands(
                        new CommandAPICommand("attack")
                                .withArguments(new StringArgument("team"))
                                .executes((sender, args) -> {
                                    Team team = teamManager.getTeamByMember((Player) sender);
                                    if(team != null){
                                        if(team.getLeader() == (Player) sender){
                                            if(teamManager.teamExists((String)args.get(0))){
                                                Siege siege = new Siege(teamManager.getTeamByName((String)args.get(0)),team, plugin);
                                                siege.start(1);
                                            }
                                        }
                                    }
                                })
                )
                // We want to target blocks in particular, so use BLOCK_POSITION
                .withSubcommand(teamCommands.getTeamCommands())
                .register();
    }
}
