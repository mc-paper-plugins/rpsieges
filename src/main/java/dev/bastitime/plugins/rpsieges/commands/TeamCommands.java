package dev.bastitime.plugins.rpsieges.commands;

import dev.bastitime.plugins.rpsieges.RPSieges;
import dev.bastitime.plugins.rpsieges.STATICS;
import dev.bastitime.plugins.rpsieges.helper.ChatHelper;
import dev.bastitime.plugins.rpsieges.manager.TeamManager;
import dev.bastitime.plugins.rpsieges.models.Invite;
import dev.bastitime.plugins.rpsieges.models.Team;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.CommandTree;
import dev.jorel.commandapi.arguments.CommandArgument;
import dev.jorel.commandapi.arguments.PlayerArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.checkerframework.checker.units.qual.C;

import java.util.HashMap;
import java.util.List;

public class TeamCommands {
    private final RPSieges plugin;
    private final TeamManager teamManager;

    private HashMap<String, Invite> invitedPlayers = new HashMap<String, Invite>();

    public TeamCommands(RPSieges plugin, TeamManager teamManager) {
        this.plugin = plugin;
        this.teamManager = teamManager;
    }

    public CommandAPICommand getTeamCommands(){
        return new CommandAPICommand("team")
                .withSubcommands(
                        new CommandAPICommand("create")
                                .withArguments(new StringArgument("name"))
                                .executes((sender, args) -> {
                                    Team newTeam = new Team((String) args.get(0), (Player) sender);
                                    if(!teamManager.teamExists((String)args.get(0))) {
                                        teamManager.addTeam(newTeam);
                                        ChatHelper.sendInfo(sender, "Du hast das Team " + STATICS.softRed + newTeam.getName() + STATICS.white + " <#ededed>erstellt.");
                                    } else {
                                        ChatHelper.sendError(sender, "Dieses Team existiert bereits.");
                                    }
                                }),
                        new CommandAPICommand("leave")
                                .executes((sender, args) -> {
                                    Team team = teamManager.getTeamByMember((Player) sender);
                                    if(team != null){
                                        if(team.getMembers().size() <= 1){
                                            teamManager.removeTeam(team);
                                            ChatHelper.sendInfo(sender,"Du hast das Team verlassen. Das Team wurde gelöscht.");
                                        } else {
                                            team.removePlayer((Player) sender);
                                            ChatHelper.sendInfo(sender, "Du hast das Team verlassen.");
                                            for (Player member: team.getMembers()) {
                                                ChatHelper.sendInfo(member,STATICS.green + sender.getName() + STATICS.white + " hat das Team verlassen.");
                                            }
                                        }
                                    }
                                }),
                        new CommandAPICommand("invite")
                                .withArguments(new PlayerArgument("player"))
                                .executes((sender, args) -> {
                                    Team team = teamManager.getTeamByMember((Player) sender);
                                    if(team != null){
                                        if(team.getLeader() == sender){
                                            Player invitedPlayer = (Player)args.get(0);
                                            if(teamManager.playerHasTeam(invitedPlayer)){
                                                ChatHelper.sendError(sender, invitedPlayer.getName() + " ist bereits in einem Team.");
                                            } else {
                                                if (invitedPlayers.containsKey(invitedPlayer.getName())) {
                                                    ChatHelper.sendError(sender, "Dieser Spieler wurde bereits von einem Team eingeladen.");
                                                } else {
                                                    Invite invite = new Invite(((60 * 1000) + System.currentTimeMillis()), team.getName());
                                                    invitedPlayers.put(invitedPlayer.getName(), invite);
                                                    ChatHelper.sendInfo(invitedPlayer, "Du wurdest zum Team " + STATICS.softRed + team.getName() + STATICS.white + " eingeladen");
                                                    ChatHelper.sendInfo(invitedPlayer, "60 Sekunden verbleibend um mit "
                                                            + STATICS.lightGray + "/sieges team accept " + team.getName() + " <insert:/sieges team accept " + team.getName() + ">"
                                                            + STATICS.darkGray + "[" + STATICS.green + "anzunehmen"
                                                            + STATICS.darkGray + "]</insert> "
                                                            + STATICS.white + "oder mit "
                                                            + STATICS.lightGray + "/sieges team deny " + team.getName() + " <insert:/sieges team deny " + team.getName() + ">"
                                                            + STATICS.darkGray + "[" + STATICS.red + "abzulehnen"
                                                            + STATICS.darkGray + "].</insert>");
                                                    ChatHelper.sendInfo(sender, "Du hast " + invitedPlayer.getName() + " eingeladen.");
                                                }
                                            }
                                        } else {
                                            ChatHelper.sendError(sender, "Du bist nicht der Leader des Teams.");
                                        }
                                    } else {
                                        ChatHelper.sendError(sender, "Du bist in keinem Team.");
                                    }
                                }),
                        new CommandAPICommand("accept")
                                .withArguments(new StringArgument("team"))
                                .executes((sender, args) -> {
                                    if(invitedPlayers.containsKey(sender.getName())){
                                        Invite invite = invitedPlayers.get(sender.getName());
                                        if (invite.End >= System.currentTimeMillis()) { //Einladung gültig
                                            invitedPlayers.remove(sender.getName());
                                            Team team = teamManager.getTeamByName(invite.Team);
                                            team.addPlayer((Player)sender);
                                            for (Player member: team.getMembers()) {
                                                ChatHelper.sendInfo(member,STATICS.green + sender.getName() + STATICS.white + " ist dem Team beigetreten.");
                                            }
                                        } else { // Einladung abgelaufen
                                            invitedPlayers.remove(sender.getName());
                                            ChatHelper.sendError(sender,"Die Einladung ist abgelaufen.");
                                        }
                                    } else {
                                        ChatHelper.sendError(sender, "Du hast keine Einladung zu diesem Team.");
                                    }
                                }),
                        new CommandAPICommand("deny")
                                .withArguments(new StringArgument("team"))
                                .executes((sender, args) -> {
                                    if(invitedPlayers.containsKey(sender.getName())){
                                        Invite invite = invitedPlayers.get(sender.getName());
                                        if (invite.End >= System.currentTimeMillis()) { //Einladung gültig
                                            invitedPlayers.remove(sender.getName());
                                            Team team = teamManager.getTeamByName(invite.Team);
                                            ChatHelper.sendInfo(team.getLeader(),STATICS.red + sender.getName() + STATICS.white + " hat die Einladung abgelehnt.");
                                        } else { // Einladung abgelaufen
                                            invitedPlayers.remove(sender.getName());
                                            ChatHelper.sendError(sender,"Die Einladung ist abgelaufen.");
                                        }
                                    } else {
                                        ChatHelper.sendError(sender, "Du hast keine Einladung zu diesem Team.");
                                    }
                                }),
                        new CommandAPICommand("list")
                                .executes((sender, args) -> {
                                    List<Team> teams = teamManager.getTeams();
                                    ChatHelper.sendInfo((Player)sender,"Liste aller Teams (" + teams.size() + ")");
                                    for (Team team: teams) {
                                        ChatHelper.sendInfo((Player)sender, "- " + team.getName() + " (" + team.getMembers().size() + ")");
                                    }
                                })
            ).executes((sender, args) -> {
                Team team = teamManager.getTeamByMember((Player) sender);
                if(team != null) {
                    ChatHelper.sendInfo(sender, "Du bist derzeit im Team " + STATICS.softRed + team.getName());
                    for (Player member: team.getMembers()) {
                        ChatHelper.sendInfo(sender, "- " + member.getName());
                    }
                } else {
                    ChatHelper.sendInfo(sender, "Du bist derzeit in keinem Team. Erstelle eins mit <insert:/sieges team create [name]>/sieges team create [name]</insert> oder nimm eine Einladung an.");
                }
            });
    }
}

