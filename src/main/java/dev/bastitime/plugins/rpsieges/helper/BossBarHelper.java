package dev.bastitime.plugins.rpsieges.helper;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class BossBarHelper {

    private @Nullable BossBar activeBar;

    public void showBossBar(String text, final @NonNull ArrayList<Audience> targets) {
        final Component name = Component.text(text);
        // Creates a red boss bar which has no progress and no notches
        final BossBar fullBar = BossBar.bossBar(name, 1, BossBar.Color.RED, BossBar.Overlay.NOTCHED_20);

        // Send a bossbar to your audience
        for (Audience target: targets) {
            target.showBossBar(fullBar);
        }
        // Store it locally to be able to hide it manually later
        this.activeBar = fullBar;
    }

    public void updateProgress(float progress){
        activeBar.progress(progress);
    }

    public void updateText(String text){
        activeBar.name(Component.text(text));
    }

    public void hideActiveBossBar(final @NonNull ArrayList<Audience> targets) {
        for (Audience target: targets) {
            target.hideBossBar(this.activeBar);
        }
        this.activeBar = null;
    }

}
