package dev.bastitime.plugins.rpsieges.helper;

import dev.bastitime.plugins.rpsieges.STATICS;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatHelper {

    public static void sendInfo(Player player, String miniMessage){
        MiniMessage mm = MiniMessage.miniMessage();
        Component parsed = mm.deserialize(STATICS.prefix + "<#ededed>" + miniMessage);
        player.sendMessage(parsed);
    }

    public static void sendInfo(CommandSender sender, String miniMessage){
        MiniMessage mm = MiniMessage.miniMessage();
        Component parsed = mm.deserialize(STATICS.prefix + "<#ededed>" + miniMessage);
        sender.sendMessage(parsed);
    }

    public static void sendError(Player player, String miniMessage){
        MiniMessage mm = MiniMessage.miniMessage();
        Component parsed = mm.deserialize(STATICS.prefix + "<#d62a1e>" + miniMessage);
        player.sendMessage(parsed);
    }

    public static void sendError(CommandSender sender, String miniMessage){
        MiniMessage mm = MiniMessage.miniMessage();
        Component parsed = mm.deserialize(STATICS.prefix + "<#d62a1e>" + miniMessage);
        sender.sendMessage(parsed);
    }
}
