package dev.bastitime.plugins.rpsieges.manager;

import dev.bastitime.plugins.rpsieges.models.Invite;
import dev.bastitime.plugins.rpsieges.models.Team;
import org.bukkit.entity.Player;
import org.checkerframework.checker.units.qual.A;

import java.util.*;

public class TeamManager {
    private ArrayList<Team> teams = new ArrayList<Team>();

    public TeamManager(){
    }

    public boolean playerHasTeam (Player player){
        for (Team team: teams) {
            if(team.isMember(player))
                return true;
        }
        return false;
    }

    public Team getTeamByMember(Player player){
        for (Team team: teams) {
            if(team.isMember(player))
                return team;
        }
        return null;
    }

    public boolean teamExists(String name){
        for (Team team: teams) {
            if(team.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public Team getTeamByName(String name){
        for (Team team: teams) {
            if(team.getName().equalsIgnoreCase(name)){
                return team;
            }
        }
        return null;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public void addTeam(Team team){
        teams.add(team);
    }

    public void removeTeam(Team team){
        teams.remove(team);
    }
}
