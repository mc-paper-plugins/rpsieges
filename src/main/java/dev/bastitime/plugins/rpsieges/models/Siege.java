package dev.bastitime.plugins.rpsieges.models;

import dev.bastitime.plugins.rpsieges.RPSieges;
import dev.bastitime.plugins.rpsieges.helper.BossBarHelper;
import dev.bastitime.plugins.rpsieges.misc.Countdown;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.audience.Audiences;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.checkerframework.checker.units.qual.A;

import java.time.Duration;
import java.time.Period;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;

public class Siege {

    private Team defender;
    private Team attacker;

    private ArrayList<Audience> audiences = new ArrayList<>();

    private RPSieges plugin;

    private BossBarHelper bossBarHelper = new BossBarHelper();

    public Siege(Team defender, Team attacker, RPSieges plugin) {
        this.defender = defender;
        this.attacker = attacker;
        this.plugin = plugin;
    }

    public void start(int minutes){

        for (Player defendPlayer: defender.getMembers()) {
            Audience audience = this.plugin.adventure().player(defendPlayer);
            audiences.add(audience);
        }
        for (Player attackPlayer: attacker.getMembers()) {
            Audience audience = this.plugin.adventure().player(attackPlayer);
            audiences.add(audience);
        }

        bossBarHelper.showBossBar(defender.getName() + " vs. " + attacker.getName() + " | verbleibende Zeit: --.--", audiences);
        new Countdown(minutes * 60, plugin) {

            @Override
            public void count(int current) {
                float progress = (float) current / (minutes*60);
                bossBarHelper.updateProgress(progress);
                if (current == 1){
                    bossBarHelper.hideActiveBossBar(audiences);
                }
                String remaining = String.format("%d:%02d:%02d", current / 3600, (current % 3600) / 60, (current % 60));
                bossBarHelper.updateText(defender.getName() + " vs. " + attacker.getName() + " | verbleibende Zeit: " + remaining);
            }
        }.start();
    }

    public Team getDefender() {
        return defender;
    }

    public void setDefender(Team defender) {
        this.defender = defender;
    }

    public Team getAttacker() {
        return attacker;
    }

    public void setAttacker(Team attacker) {
        this.attacker = attacker;
    }
}
