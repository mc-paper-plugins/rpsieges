package dev.bastitime.plugins.rpsieges.models;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Team {

    private String name;
    private ArrayList<Player> members = new ArrayList<>();
    private Player leader;

    public Team (String name, Player leader){
        this.name = name;
        this.leader = leader;
        this.members.add(leader);
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public ArrayList<Player> getMembers(){
        return members;
    }

    public void addPlayer(Player player){
        members.add(player);
    }

    public void removePlayer(Player player) {
        this.members.remove(player);
    }

    public boolean isMember(Player player){
        return members.contains(player);
    }

    public Player getLeader(){
        return leader;
    }
}
